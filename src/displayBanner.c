#include <classification.h>

/*
 * We ignore the quit on purpose
 */
gboolean main_quit (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
  gtk_widget_show_all(widget);
  return TRUE;
}

GtkWidget *window;
void displayBanner( ClassificationInfo info, int argc, char *argv[])
{
  GtkWidget *grid, *label, *hostLabel, *userLabel;
  GdkColor color;
  GdkRectangle workarea = {0};
  gint width;
  char userNameLabel[256], hostNameLabel[256];


  gtk_init(&argc, &argv);

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  /* Set the color from the config file. Default: green */
  gdk_color_parse (info.color, &color);

  /* Create a new grid */
  grid = gtk_grid_new ();

  if ( info.fDisplayUserName ) {
    sprintf(userNameLabel, "<b>Current User: %s </b>", getUser());
  }

  if (info.fDisplayHostName ) {
    sprintf(hostNameLabel, "<b>Host: %s </b>", getHostName());
  }

  /* Create the labels to be shown */
  userLabel = gtk_label_new("");
  gtk_label_set_markup((GtkLabel*)userLabel, userNameLabel);
  
  hostLabel = gtk_label_new("");
  gtk_label_set_markup((GtkLabel*)hostLabel, hostNameLabel);

  label = gtk_label_new ("");
  gtk_label_set_markup((GtkLabel*)label, info.message);

  /*Expand them vertically and horizontally */
  gtk_widget_set_vexpand (label, TRUE);
  gtk_widget_set_hexpand (label, TRUE);
  
  gtk_widget_set_vexpand (userLabel, TRUE);
  gtk_widget_set_hexpand (userLabel, TRUE);
  
  gtk_widget_set_vexpand (hostLabel, TRUE);
  gtk_widget_set_hexpand (hostLabel, TRUE);
  
  
  gtk_grid_insert_column((GtkGrid*) grid, 0);
  gtk_grid_attach (GTK_GRID (grid), userLabel, 0, 0, 1, 1);
  
  gtk_grid_insert_column((GtkGrid*) grid, 1);
  gtk_grid_attach (GTK_GRID (grid), label, 1, 0, 1, 1);

  gtk_grid_insert_column((GtkGrid*) grid, 2);
  gtk_grid_attach (GTK_GRID (grid), hostLabel, 2, 0, 1, 1);

  gtk_grid_set_column_spacing ((GtkGrid *)grid, (gdk_screen_width()/3));

  gtk_container_add (GTK_CONTAINER (window), grid);

  gtk_widget_modify_bg(window, GTK_STATE_NORMAL, &color);

  int wsize = gdk_screen_width();
  int hsize = gdk_screen_height();
  
  /* printf("Width: %d and Height: %d\n", wsize, hsize); */
  //gtk_window_set_default_size(GTK_WINDOW(window), wsize, 25);
  gtk_window_resize(GTK_WINDOW(window), wsize, 25);
  gtk_window_set_decorated(GTK_WINDOW(window), FALSE);



  gtk_widget_set_vexpand (window, TRUE);
  gtk_widget_set_hexpand (window, TRUE);
  
  gtk_widget_set_vexpand (grid, TRUE);
  gtk_widget_set_hexpand (grid, TRUE);

  gtk_window_stick(GTK_WINDOW(window));

  gtk_widget_show_all (window);

  gtk_window_set_keep_above (GTK_WINDOW(window), TRUE);

  /* Disable the delete event with the callback */
  g_signal_connect(window, "delete-event",
      G_CALLBACK(main_quit), NULL);  

  g_signal_connect(window, "configure-event",
      G_CALLBACK(resize), NULL);  

  g_signal_connect(window, "window-state-event",
      G_CALLBACK(icon_reset), NULL);  

  GdkScreen *screen = gtk_widget_get_screen(window);

  g_signal_connect(screen, "size-changed",
      G_CALLBACK(resizeScreen), NULL);  

}
