#include <classification.h>

/*
 * @brief Main function for the classification banner
 */
  
int main(int argc, char *argv[]) {
  int opt;
  char configFile[256];
      
  while ((opt = getopt(argc, argv, "f:")) != -1) {
    switch (opt) {
    case 'f':
      strcpy(configFile, optarg);
      printf("File: %s\n", configFile);
      break;
    default: /* '?' */
      fprintf(stderr, "Usage: %s [-f config]\n",
	      argv[0]);
      exit(EXIT_FAILURE);
    }
  }

  if (configFile[0] == '\0' ) {
    strcpy(configFile, "/etc/classification-banner/config.ini");
  }
  ClassificationInfo info = readBannerConfig(configFile);

  displayBanner(info, argc, argv);

  gtk_main();
  
  return 0;
}
