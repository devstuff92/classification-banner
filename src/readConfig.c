#include <classification.h>

ClassificationInfo readBannerConfig( const char* configFile )
{
  dictionary* contents;
  ClassificationInfo info;
  
  contents = iniparser_load(configFile);

  strcpy(info.message, iniparser_getstring(contents, "banner:message", "NO CLASSIFICATION"));
  strcpy(info.color, iniparser_getstring(contents, "banner:color", "green"));

  info.fDisplayUserName = (iniparser_getint(contents, "banner:display_user", 0) == 1) ? true : false;
  info.fDisplayHostName = (iniparser_getint(contents, "banner:display_hostname", 0) == 1) ? true : false;

  /* printf("Color: %s Message: %s user: %d host: %d\n", info.color, info.message, info.fDisplayUserName, info.fDisplayHostName); */
  
  iniparser_freedict( contents );

  return info;
}
