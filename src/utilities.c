#include <classification.h>

char hostname[HOST_NAME_MAX + 1];


void printDisplayInfo()
{
  GdkDisplay *display;

  display = gdk_display_get_default();

  int numDisplays = gdk_display_get_n_screens(display);

  printf("Num Screens = %d\n", numDisplays);
}

const char* getUser()
{
  register struct passwd *pw;
  register uid_t uid;

  uid = geteuid (); /* Get the effective uid */
  pw = getpwuid (uid); /* returns a pointer to a structure for record in the  password  database */
  if (pw) {
    return pw->pw_name; /* Just return the user name */
  }
  return ""; /* We must've not found the userid? Should never happen. */
}

const char* getHostName()
{
  gethostname(hostname, HOST_NAME_MAX + 1); 

  return (const char*) hostname;
}
