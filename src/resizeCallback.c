#include <classification.h>

extern GtkWidget *window; /* We make this global so we can resize it correctly */

gboolean icon_reset(GtkWidget *widget, GdkEventWindowState *event, gpointer user_data)
{
        if(event->changed_mask == GDK_WINDOW_STATE_ICONIFIED && (event->new_window_state == GDK_WINDOW_STATE_ICONIFIED || event->new_window_state == (GDK_WINDOW_STATE_ICONIFIED | GDK_WINDOW_STATE_MAXIMIZED)))
        {
          return FALSE;
        }
        else if(event->changed_mask == GDK_WINDOW_STATE_WITHDRAWN && (event->new_window_state == GDK_WINDOW_STATE_ICONIFIED || event->new_window_state == (GDK_WINDOW_STATE_ICONIFIED | GDK_WINDOW_STATE_MAXIMIZED)))
        {
          printf("Withdrawn\n");
          return FALSE;
        }
        return TRUE;
}

gboolean resize (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
  printf("Event Window Resize x: %d y: %d\n", event->configure.x, event->configure.y);
  printf("Event Window Resize width: %d height: %d\n", event->configure.width, event->configure.height);
  printf("Send Event boolean: %d \n", event->configure.send_event);
  printf("Screen Width: %d \n", gdk_screen_width());
  event->configure.width = gdk_screen_width();
  printf("Window Resize width: %d\n", gdk_screen_width());
  gtk_window_resize( (GtkWindow*)widget , event->configure.width, 25);
  gtk_widget_show_all (widget);
  gtk_widget_grab_focus(widget);
  gtk_window_set_keep_above (GTK_WINDOW(widget), TRUE);

  return FALSE;
}

gboolean resizeScreen (GdkScreen *widget, GdkEvent *event, gpointer user_data)
{
   if(GDK_IS_SCREEN(widget))
   {
     printf("resizing Window argument not NULL\n");
     gtk_window_resize( (GtkWindow*)window, gdk_screen_width(), 25);
     gtk_widget_show_all (window);
     gtk_widget_grab_focus(window);
     gtk_window_set_keep_above (GTK_WINDOW(window), TRUE);
   }
   return FALSE;
}
