/** @file classification.h
 *  @brief Function prototypes and definitions for classification banner
 *
 *  This contains the prototypes for the classification
 *  banner and any macros, constants,
 *  or global variables you will need.
 *
 *  @author Red Hat Inc
 *  @bug No known bugs.
 */

#ifndef	_CLASSIFICATION_H
#define	_CLASSIFICATION_H	1

#include <stdlib.h>
#include <pwd.h>
#include <stdio.h>
#include <unistd.h>

/* GTK Include */
#include <gtk/gtk.h>

/*
 * Required to read INI config file
 * dnf/yum install iniparser iniparser-devel
 */
#include <iniparser.h>

#define MAXSIZE 32
#define MAXMSG  256

typedef enum {false, true} bool;

/**
 * @brief Classification structure
 *
 * This is the structure that holds all the members of the classification banner
 */
typedef struct Classification_struct {
  const char hostname[HOST_NAME_MAX + 1 ];
  const char username[MAXSIZE];
  char message[MAXMSG];
  char color[MAXSIZE];
  bool fDisplayUserName;
  bool fDisplayHostName;
} ClassificationInfo;
  
/*
 * @brief Function Prototypes
 */

void printDisplayInfo();

/**
 * @brief getUser retrieves the current User Id
 *
 * The getUser() function retrieves the current User Id which will be added to the classification bar
 * 
 * @param void
 *
 * @return const char* Returns the username as a const char*
 */
const char* getUser();

/**
 * @brief getUser retrieves the current User Id
 *
 * The getUser() function retrieves the current User Id which will be added to the classification bar
 * 
 * @param void
 *
 * @return Describe what the function returns.
 */

const char* getHostName();

/**
 * @brief getUser retrieves the current User Id
 *
 * The getUser() function retrieves the current User Id which will be added to the classification bar
 * 
 * @param void
 *
 * @return Describe what the function returns.
 */
ClassificationInfo readBannerConfig( const char* configFile );

/**
 * @brief DisplayBanner
 *
 * The displayBanner() function retrieves the current User Id which will be added to the classification bar
 * 
 * @param info - ClassificationInfo structure
 * @param argc - Number of command line arguments
 * @param argv - Command line argument array
 *
 * @return void
 */
void displayBanner( ClassificationInfo info, int argc, char *argv[]);

//gboolean resizeScreen (GtkWidget *widget, GdkEvent *event, gpointer user_data);
//gboolean resizeScreen (GdkWindow *widget, GdkEvent *event, gpointer user_data);
gboolean icon_reset(GtkWidget *widget, GdkEventWindowState *event, gpointer user_data);
gboolean resizeScreen (GdkScreen *widget, GdkEvent *event, gpointer user_data);

gboolean resize (GtkWidget *widget, GdkEvent *event, gpointer user_data);
/*
 * @brief constants and global vars
 */

#endif /* _CLASSIFICATION_H  */
