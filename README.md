# Classification Banner

Classification banner for use on linux systems

## Requirements:

We use the iniparser package to parse the config.ini file.
You will have to install the following package

On Fedora:
````
$ sudo dnf install initparser iniparser-devel gcc gtk3-devel make
````

On RHEL 7:
````
$ sudo yum install initparser iniparser-devel gcc gtk3-devel make
````

## Compiling the code

You will need the following packages installed for development:

Fedora:
```
$ dnf install gtk3-devel-3.22.30-5.el7.x86_64
```

RHEL 7:
```
$ yum install gtk3-devel-3.22.30-5.el7.x86_64
```

Also, make sure you have the **make** utility installed and the compiler e.g. **gcc**.

First you will need to clone to your local host by doing the following:

```
[claudiol@satellite aurora]$ git clone https://gitlab.com/devstuff92/classification-banner.git
Cloning into 'classification-banner'...
Username for 'https://gitlab.com': claudiol1
Password for 'https://claudiol1@gitlab.com': 
remote: Enumerating objects: 37, done.
remote: Counting objects: 100% (37/37), done.
remote: Compressing objects: 100% (34/34), done.
remote: Total 37 (delta 10), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (37/37), done.
[claudiol@satellite aurora]$ cd classification-banner/
[claudiol@satellite classification-banner]$
```



To compile just type the following on the command line:

````
[claudiol@fedora classification-banner]$ make
gcc -Wno-deprecated-declarations `pkg-config --cflags --libs gtk+-3.0`  -I ./include -c src/banner.c  -o src/banner.o
gcc -Wno-deprecated-declarations `pkg-config --cflags --libs gtk+-3.0`  -I ./include -c src/displayBanner.c  -o src/displayBanner.o
gcc -Wno-deprecated-declarations `pkg-config --cflags --libs gtk+-3.0`  -I ./include -c src/readConfig.c  -o src/readConfig.o
gcc -Wno-deprecated-declarations `pkg-config --cflags --libs gtk+-3.0`  -I ./include -c src/utilities.c  -o src/utilities.o
gcc -Wno-deprecated-declarations `pkg-config --cflags --libs gtk+-3.0`  -I ./include -o banner src/banner.o src/displayBanner.o src/readConfig.o src/utilities.o  -liniparser
Compiled banner
[claudiol@fedora classification-banner]$ 
````
## Files in the distro

The files in the distribution are:

| File | Description |
--- | --- |
config.ini | Config file for the classification banner
include/classification.h | Main header file
src/banner.c | Contains the main function
src/displayBanner.c | Contains all the GTK+ code 
src/readConfig.c | Contains the parsing code for the config.ini file.
src/utilities.c | Contains utilities used such as getUser, getHostName functions.
xdg/banner.desktop | GNome file to copy to /etc/xdg/autostart.  This will show the banner automatically at user login


````
.
├── banner
├── config.ini
├── include
│   └── classification.h
├── Makefile
├── nohup.out
├── README.md
├── src
│   ├── banner.c
│   ├── displayBanner.c
│   ├── readConfig.c
│   ├── resizeCallback.c
│   ├── utilities.c
└── xdg
    ├── banner.desktop
    └── README.md

3 directories, 18 files
````

## NOTES

This should compile on the following platforms

* Fedora 32
* RHEL 7

Make sure you have the optional repository enabled.

```
[root@satellite ~]# subscription-manager repos --enable rhel-7-server-optional-rpms
```

That will give you access to the **iniparser-devel** package.

* RHEL 8

```
[claudiol@rhel8 classification-banner]$ sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
[sudo] password for claudiol: 
Updating Subscription Management repositories.
Last metadata expiration check: 0:04:43 ago on Fri 18 Sep 2020 02:47:09 PM MDT.
epel-release-latest-8.noarch.rpm                                                          64 kB/s |  22 kB     00:00    
Dependencies resolved.
=========================================================================================================================
 Package                        Architecture             Version                    Repository                      Size
=========================================================================================================================
Installing:
 epel-release                   noarch                   8-8.el8                    @commandline                    22 k

Transaction Summary
=========================================================================================================================
Install  1 Package

Total size: 22 k
Installed size: 32 k
Is this ok [y/N]: y
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                 1/1 
  Installing       : epel-release-8-8.el8.noarch                                                                     1/1 
  Running scriptlet: epel-release-8-8.el8.noarch                                                                     1/1 
  Verifying        : epel-release-8-8.el8.noarch                                                                     1/1 
Installed products updated.

Installed:
  epel-release-8-8.el8.noarch                                                                                            

Complete!
[claudiol@rhel8 classification-banner]$ sudo dnf repolist
Updating Subscription Management repositories.
repo id                                          repo name
epel                                             Extra Packages for Enterprise Linux 8 - x86_64
epel-modular                                     Extra Packages for Enterprise Linux Modular 8 - x86_64
google-chrome                                    google-chrome
rhel-8-for-x86_64-appstream-rpms                 Red Hat Enterprise Linux 8 for x86_64 - AppStream (RPMs)
rhel-8-for-x86_64-baseos-rpms                    Red Hat Enterprise Linux 8 for x86_64 - BaseOS (RPMs)
[claudiol@rhel8 classification-banner]$
```

The **iniparser** is in EPEL for RHEL 8.  For RHEL 8 we might have to create an iniparser package that can be used.  This is a TODO for our team.


Make sure the iniparser-devel is installed 

```
[claudiol@rhel8 classification-banner]$ sudo dnf install iniparser-devel
Updating Subscription Management repositories.
Last metadata expiration check: 0:06:50 ago on Fri 18 Sep 2020 02:52:58 PM MDT.
Dependencies resolved.
=========================================================================================================================
 Package                            Architecture              Version                      Repository               Size
=========================================================================================================================
Installing:
 iniparser-devel                    x86_64                    4.1-5.el8                    epel                     16 k

Transaction Summary
=========================================================================================================================
Install  1 Package

Total download size: 16 k
Installed size: 21 k
Is this ok [y/N]: y
Downloading Packages:
iniparser-devel-4.1-5.el8.x86_64.rpm                                                      55 kB/s |  16 kB     00:00    
-------------------------------------------------------------------------------------------------------------------------
Total                                                                                     23 kB/s |  16 kB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                 1/1 
  Installing       : iniparser-devel-4.1-5.el8.x86_64                                                                1/1 
  Running scriptlet: iniparser-devel-4.1-5.el8.x86_64                                                                1/1 
  Verifying        : iniparser-devel-4.1-5.el8.x86_64                                                                1/1 
Installed products updated.

Installed:
  iniparser-devel-4.1-5.el8.x86_64                                                                                       

Complete!
[claudiol@rhel8 classification-banner]$ make
gcc -Wno-deprecated-declarations `pkg-config --cflags --libs gtk+-3.0`  -I ./include -c src/banner.c  -o src/banner.o
gcc -Wno-deprecated-declarations `pkg-config --cflags --libs gtk+-3.0`  -I ./include -c src/displayBanner.c  -o src/displayBanner.o
gcc -Wno-deprecated-declarations `pkg-config --cflags --libs gtk+-3.0`  -I ./include -c src/readConfig.c  -o src/readConfig.o
gcc -Wno-deprecated-declarations `pkg-config --cflags --libs gtk+-3.0`  -I ./include -c src/utilities.c  -o src/utilities.o
gcc -Wno-deprecated-declarations `pkg-config --cflags --libs gtk+-3.0`  -I ./include -o banner src/banner.o src/displayBanner.o src/readConfig.o src/utilities.o  -liniparser
Compiled banner
[claudiol@rhel8 classification-banner]$ 
```
