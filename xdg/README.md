# Desktop file for gnome desktop

the banner.xdg can be copied to the /etc/xdg/autostart directory to enable the classification banner when the user logs in.

## Installing

1 - Copy the banner.xdg file to the /etc/xdg/autostart directory.

2 - Compile the banner distribution.

3 - Copy the banner executable to /usr/local/bin

